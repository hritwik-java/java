import java.util.Scanner;

class Program_8{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+", ");

		}

		System.out.println();

		int min = arr[0];
		int min2 = arr[1];


		if(min2<min){
			int temp = min;
			min = min2;
			min2 = temp;
		}

		for(int i=2;i<arr.length;i++){

			if(arr[i]<min){
				min2 =min;
				min = arr[i];
			}else if(arr[i]<min2 && arr[i]!=min){
				min2=arr[i];
			}
		}

		System.out.println("Second Minimum number in an Array is: " + min2);
		
	}
}
