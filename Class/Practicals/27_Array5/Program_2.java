import java.util.Scanner;

class Program_2{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+", ");

		}

		System.out.println();


		int oddSum = 0;
		int evenSum = 0;

		for(int i=0;i<arr.length;i++){

			if(arr[i]%2==0){

				evenSum = evenSum + arr[i];

			}else{

				oddSum = oddSum + arr[i];
			}
		}

		System.out.println("Odd Sum= " +oddSum);
		System.out.println("Even Sum= "+evenSum);
	}
}
