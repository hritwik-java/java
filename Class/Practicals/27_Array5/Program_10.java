import java.util.Scanner;

class Program_10{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+", ");

		}

		System.out.println();


		for(int i=0;i<arr.length;i++){
			
			int fact=1;
			while(arr[i]!=0){
				fact = fact * arr[i];
				arr[i]--;
			}

			System.out.println(fact);
		}
		System.out.println();
		
	}
}
