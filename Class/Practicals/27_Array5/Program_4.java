import java.util.Scanner;

class Program_4{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+", ");

		}

		System.out.println();

		int flag=0;

		for(int i=0;i<arr.length;i++){

			for(int j=0;j<arr.length;j++){


				if(arr[i]==arr[j] && i != j ){

					System.out.print("First duplicate element found at index "+i);
					flag=1;
					break;

				}
			}
			if(flag==1){
				break;
			}
		}

		System.out.println();
		
	}
}
