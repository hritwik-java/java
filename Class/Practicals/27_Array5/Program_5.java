import java.util.Scanner;

class Program_5{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+", ");

		}

		System.out.println();

		for(int i=0;i<arr.length;i++){

			int rem=0;
			int cnt=0;

			while(arr[i]!=0){
				rem = arr[i] % 10 ;
				cnt++;
				arr[i] = arr[i] / 10 ;
			}
			
			System.out.print(cnt+", ");	

		}

		System.out.println();
		
	}
}
