import java.util.Scanner;

class Program_9{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a Number: ");
		int num = sc.nextInt();
		int temp = num;

		int cnt=0;
		int rev=0;
		int rem=0;

		while(temp!=0){
			rem = temp % 10;
			cnt++;
			rev=  rev*10+rem;
			temp = temp /10;
		}

		int arr[] = new int[cnt];

		System.out.println("Array Data After Adding 1 : ");
		for(int i=0;i<arr.length && rev!=0;i++){

			rem = rev % 10 ;

			arr[i] = rem + 1 ;

			rev = rev / 10 ;

			System.out.print(arr[i] +", ");

		}
		System.out.println();
		
	}
}
