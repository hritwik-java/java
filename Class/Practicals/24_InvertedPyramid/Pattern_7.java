import java.util.Scanner;

class Pattern_7 {
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number of Rows: ");
		int row = sc.nextInt();

		int temp=row;
		System.out.println("Pattern : ");

		for(int i=1;i<=row;i++){
			char CH='A';
			for(int space=1;space<=i-1;space++){

				System.out.print("\t");
			}
			for(int j=1;j<=temp*2-1;j++){

				if(j>(temp*2-1)/2){
					System.out.print(CH-- +"\t");
				}else{
					System.out.print(CH++ +"\t");
				}
			}
			temp--;
			System.out.println();
		}
	}
}

