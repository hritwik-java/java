import java.util.Scanner;

class Pattern_2 {
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number of Rows: ");
		int row = sc.nextInt();

		int temp=row;
		int n=1;

		System.out.println("Pattern : ");

		for(int i=1;i<=row;i++){
			for(int space=1;space<=i-1;space++){

				System.out.print("\t");
			}
			for(int j=1;j<=temp*2-1;j++){
				System.out.print(n++ +"\t");
			}
			temp--;
			System.out.println();
		}
	}
}

