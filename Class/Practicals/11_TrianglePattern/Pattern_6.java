import java.io.*;

class Pattern_6{
	public static void main(String[] args) throws IOException{
		 
		BufferedReader br = new BufferedReader(new InputStreamReader (System.in));

		System.out.print("Enter Number of rows: ");
		int row = Integer.parseInt(br.readLine());

		int n=row;
		for(int i=1;i<=row;i++){
			for(int j=row;j>=i;j--){
				System.out.print(n+" ");
			}
			n--;
			System.out.println();
		}
	}

}
			
