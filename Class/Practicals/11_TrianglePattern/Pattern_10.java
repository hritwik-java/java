import java.io.*;

class Pattern_10{
	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Number of rows: ");
		int row = Integer.parseInt(br.readLine());


		for(int i=1;i<=row;i++){
			int ch = 64+i;

			for (int j=row;j>=i;j--){
				if (i%2==1){
					if(j%2==1){
						System.out.print((char)ch +" ");
					}else{
						System.out.print(ch +" ");
					}
				}else{
					if(j%2==1){
						System.out.print(ch +" ");
					}else{
						System.out.print((char)ch +" ");
					}
				}
				ch++;
			}
			System.out.println();
		}
	}
}

