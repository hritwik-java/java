import java.io.*;

class Pattern_7{
	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader( new InputStreamReader (System.in));

		System.out.print("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			int n =1;
			for(int j=row;j>=i;j--){
				System.out.print(n++ +" ");
			}
		System.out.println();
		}
	}
}

