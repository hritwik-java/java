import java.io.*;

class Pattern_2{
	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for (int i=1;i<=row;i++){
			int n =row ;
			for(int j=1;j<=i;j++){
				System.out.print(n-- + " ");
			}
			System.out.println();
		}
	}
}
