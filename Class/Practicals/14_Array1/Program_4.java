import java.io.*;

class Program_4 {
	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());

		int sum=0;
		int arr[] = new int[size];

		System.out.println("Enter Elemnts of array : ");
		for(int i=0; i<=arr.length-1; i++){
			
			arr[i] = Integer.parseInt(br.readLine());

		}

		for(int i=0;i<=arr.length-1;i++){

			if(arr[i] %2==1){
				sum = sum+arr[i];
			}
		}
		System.out.println("Sum of odd elements of array iss: "+ sum);
	}
}
