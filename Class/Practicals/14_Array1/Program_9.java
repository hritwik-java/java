import java.util.Scanner;

class Program_9 {
	public static void main(String[] args){


		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of array: ");
		int size = sc.nextInt();

		int Arr[] = new int[size];

		for(int i=0;i<Arr.length;i++){

			System.out.print("Enter element" +(i+1)+ " : " );

			Arr[i] = sc.nextInt();
		}


		for(int i=0;i<Arr.length;i++){
			if(i%2==1){

				System.out.println("Odd index element is: "+Arr[i]);
			}
		}

		System.out.println();
	}
}

