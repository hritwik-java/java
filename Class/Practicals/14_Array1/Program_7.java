import java.io.*;

class Program_7 {
	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Elemnts of array : ");
		for(int i=0; i<=arr.length-1; i++){
			
			arr[i] = Integer.parseInt(br.readLine());

		}
		System.out.print("Array elements which are divisible by 4 are: ");

		for(int i=0;i<=arr.length-1;i++){

			if(arr[i] %4==0){
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();
	}
}
