import java.io.*;

class Program_2 {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a size of Array: ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Elements of array: ");

		for(int i=0; i<=arr.length-1; i++){

			arr[i] = Integer.parseInt(br.readLine());

		}

		System.out.println("Array Elemnts are: ");
		for(int i=0; i<=arr.length-1; i++){
			
			System.out.println(arr[i]);
		}
	}
}

