import java.util.Scanner;

class Program_8 {
	public static void main(String[] args){


		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number of Employees: ");
		int cnt = sc.nextInt();

		int ageArr[] = new int[cnt];

		for(int i=0;i<ageArr.length;i++){

			System.out.print("Enter age of Employee number " +(i+1)+ " : " );

			ageArr[i] = sc.nextInt();
		}


		for(int i=0;i<ageArr.length;i++){

			System.out.println("Age of Employee number "+ (i+1) +" is : "+ageArr[i]);
		}

		System.out.println();
	}
}

