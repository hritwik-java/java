import java.util.Scanner;

class Program_10 {

	public static void main(String args[]){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of students: ");
		int size = sc.nextInt();

		int marks[] = new int[size];

		System.out.println();

		for(int i=0;i<marks.length;i++){

			System.out.print("Enter marks of studet "+(i+1) + " : ");
			marks[i] = sc.nextInt();

		}

		System.out.println("Marks of Student are: ");

		for(int i=0;i<marks.length;i++){

			System.out.println(marks[i]);
		}
	}

}
