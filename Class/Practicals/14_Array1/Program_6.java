import java.util.Scanner;

class Program_6 {
	public static void main(String[] args) {

		Scanner sc= new Scanner(System.in);

		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter Elemnts of array : ");
		for(int i=0; i<=arr.length-1; i++){
			
			arr[i] = sc.next().charAt(0);
		}
		
		System.out.println("Array Elements are: ");

		for(int i=0;i<=arr.length-1;i++){
			
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
