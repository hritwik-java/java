import java.util.Scanner;

class Program_10{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array(size of Array must be more than 3): ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+", ");

		}

		System.out.println();

		int max = arr[0];
		int max2 = arr[0];
		int max3 = arr[0];

		for(int i=0;i<arr.length;i++){

			if(arr[i]>max){
				max=arr[i];
			}
				
		}

		for(int i=0;i<arr.length;i++){
			if(arr[i]>max2 && arr[i]!=max){
				max2=arr[i];
			}
		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]>max3 && arr[i]!= max && arr[i] != max2){
				max3 = arr[i];
			}
		}

		System.out.println("Third largest number in Array is: "+max3);
		
	}
}
