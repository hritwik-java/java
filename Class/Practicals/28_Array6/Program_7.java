import java.util.Scanner;

class Program_7{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+", ");

		}

		System.out.println();
	
		System.out.print("New Array: ");

		for(int i=0;i<arr.length;i++){

			if(arr[i]>64 && arr[i]<91){

				System.out.print((char)arr[i] +", ");
			}else{

				System.out.print(arr[i] +", ");
			}
		}

		System.out.println();
		
	}
}
