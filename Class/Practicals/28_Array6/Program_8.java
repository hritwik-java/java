import java.util.Scanner;

class Program_8 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter Elements of Array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.next().charAt(0);

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i] +", ");
		}
		System.out.println();

		System.out.print("Alternate Elements of Array Before Reverse: ");
		for(int i=0;i<arr.length;i+=2 ){

			System.out.print(arr[i] +", ");

		}

		for(int i=0;i<arr.length/2;i++){

			char temp = arr[i];
			arr[i] = arr[arr.length-i-1];
			arr[arr.length-i-1] = temp;

		}
		System.out.println();

		System.out.print("Array After Reverse: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i] +", ");
		}
		System.out.println();

		System.out.print("Alternate Elements of Array After Reverse: ");
		for(int i=0;i<arr.length;i+=2){

			System.out.print(arr[i] +", ");

		}
		System.out.println();
		
	}
}
			

		

