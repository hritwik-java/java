import java.util.Scanner;

class Program_4{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("Enter Data of Array 1: ");
		for(int i=0;i<arr1.length;i++){

			arr1[i] = sc.nextInt();
		}

		System.out.println("Enter Data of Array 2: ");
		for(int i=0;i<arr2.length;i++){
			arr2[i] = sc.nextInt();
		}
		
		
		System.out.print("Data of Array 1: ");
	
		for(int i=0;i<arr1.length;i++){
			System.out.print(arr1[i]+", ");
		}
		System.out.println();

		System.out.print("Data of Array 2: ");
		for(int i=0;i<arr2.length;i++){

			System.out.print(arr2[i]+", ");

		}

		System.out.println();

		System.out.print("Common Elements in given Arrays are: ");
		for(int i=0;i<arr1.length;i++){

			for(int j=0;j<arr2.length;j++){


				if(arr1[i] == arr2[j]){

					System.out.print(arr1[i]+" , ");
				}
			}
		}

		System.out.println();
		
	}
}
