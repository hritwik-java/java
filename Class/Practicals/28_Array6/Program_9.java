import java.util.Scanner;

class Program_9{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+", ");

		}

		System.out.println();

		int cnt=0;
		for(int i=0;i<arr.length;i++){
			
			int temp = arr[i];
			int rem=0;
			int rev=0;
			
			while(temp!=0){
				
				rem = temp % 10;
				rev = rev * 10 + rem;
				temp = temp / 10;
			}
			if(arr[i] == rev){
				cnt++;
			}
		}

		System.out.println("Count of Palindrome Elements in Array is: "+cnt);
		
	}
}
