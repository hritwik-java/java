import java.util.Scanner;

class Program_5 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);


		System.out.print("Enter size of Array 1: ");
		int size1 = sc.nextInt();

		System.out.print("Enter size of Array 2: ");
		int size2 = sc.nextInt();


		int arr1[] = new int[size1];
		int arr2[] = new int[size2];


		System.out.println("Enter Data of Array 1: ");
		for(int i=0;i<arr1.length;i++){

			arr1[i] = sc.nextInt();

		}

		System.out.println("Enter Data of Array 2: ");
		for(int i=0;i<arr2.length;i++){

			arr2[i] = sc.nextInt();

		}

		System.out.print("Data of Array 1: ");
		for(int i=0;i<arr1.length;i++){

			System.out.print(arr1[i] + " , ");
		}
		System.out.println();

		System.out.print("Data of Array 2: ");
		for(int i=0;i<arr2.length;i++){

			System.out.print(arr2[i] + " , ");

		}
		System.out.println();

		int size3 =  arr1.length + arr2.length;

		int comboArr[] =new int[size3];

		for(int i=0;i<arr1.length;i++){

			comboArr[i] = arr1[i];


		}

		for(int i=arr1.length;i<comboArr.length;i++){
		
			comboArr[i] = arr2[i-arr1.length];
		
		}

		System.out.print("Array After Merging: ");

		for(int i=0;i<comboArr.length;i++){

			System.out.print(comboArr[i] +", ");
		}

		System.out.println();
	}
}




