import java.util.Scanner;

class Program_2{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+", ");

		}

		System.out.println();

		int primeCnt=0;
		int primeSum=0;
		for(int i=0;i<arr.length;i++){
			int cnt=0;
			for(int j=1;j<=arr[i];j++){

				if(arr[i]%j==0){
					cnt++;
				}
			}
			if(cnt==2){
				primeCnt++;
				primeSum+=arr[i];
			}
		}

		System.out.println("Sum of all prime numbers is "+primeSum+" and count of prime numbers in the given array is "+primeCnt);
		
	}
}
