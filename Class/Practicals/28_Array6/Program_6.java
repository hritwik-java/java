import java.util.Scanner;

class Program_6{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Data: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+", ");

		}

		System.out.println();

		System.out.print("Enter a key: ");
		int key = sc.nextInt();

		int flag=0;
		for(int i=0;i<arr.length;i++){

			if(arr[i]%key==0){
				System.out.println("An Element multiple of " +key+" found at index: "+i);
				flag=1;		
			}
		}

		if(flag==0){
			System.out.println("Element not Found");
		}

		System.out.println();
		
	}
}
