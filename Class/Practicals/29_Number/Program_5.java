import java.util.Scanner;

class Program_5 {
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a Number: ");
		int num = sc.nextInt();

		int temp = num ;
	
		int cnt=0;
		int rem=0;
		while(temp>0){
			rem= temp%10;
			cnt++;
			temp=temp/10;
		}

		int square = num * num ;

		temp = square;
		int rev=0;
		while(cnt>0){
			rem= temp%10;
			rev=rev*10+rem;
			cnt--;
			temp= temp/10;
		}

		int rev2=0;
		while(rev>0){
			rem=rev%10;
			rev2= rev2 * 10 + rem;
			rev = rev/10;
		}

		if(rev2==num){
			System.out.println(num+ " is Automorphic number.");
		}else{
			System.out.println(num+" is Not automorphic number.");
		}
	}
}
			

