import java.util.Scanner;

class Program_2 {
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a Number: ");
		int num = sc.nextInt();

		int temp = num;
		int rem=0;
		int sum=0;

		while(temp>0){
			rem = temp % 10;
			int fact =1;
			while(rem>1){
				fact = fact* rem;
				rem--;
			}
			sum = sum + fact ;

			temp = temp /10;
		}

		if(sum == num ){
			System.out.println(num+" is Strong number.");
		}else{
			System.out.println(num+" is not strong number.");
		}
	}
}

