import java.util.Scanner;

class Pattern_3{

	public static void main(String[] args){

		Scanner sc =new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			
			int CH = 64 + row;
			int n=1;

			for(int j=1;j<=row;j++){
				if(i%2==0){
					System.out.print(n++ +" ");
				}else{
					System.out.print((char)CH-- +" ");
				}
			}
			System.out.println();
		}
	}
}
