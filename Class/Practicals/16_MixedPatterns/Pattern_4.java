import java.util.Scanner;

class Pattern_4{

	public static void main(String[] args){

		Scanner sc =new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt();

		int temp = row;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){

				System.out.print(temp*j +" ");
			}
			System.out.println();
			temp--;
		}
	}
}
