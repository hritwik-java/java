import java.util.Scanner;

class Pattern_2{

	public static void main(String[] args){

		Scanner sc =new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt();
		
		int CH = 64 + row;

		int n = row;
		
		for(int i=1;i<=row;i++){
		
			for(int j=1;j<=row;j++){

				System.out.print((char)CH+""+n-- +" ");
			}
			System.out.println();
			n = row+i;
		}
	}
}

