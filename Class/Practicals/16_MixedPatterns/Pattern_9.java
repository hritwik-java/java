import java.util.Scanner;

class Pattern_9 {
	public static void main(String[] args){

		Scanner sc= new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();
		int temp=row;
		for(int i=1;i<=row;i++){
			int n =1 ;
			int CH = 64+temp;

			for(int j=row;j>=i;j--){
				if(i%2==0){	
					System.out.print((char)CH-- +" ");
				}else{
					System.out.print(n++ +" ");
				}
			}
			temp--;
			System.out.println();
		}
	}
}
