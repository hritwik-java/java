import java.util.Scanner;

class Pattern_10 {
	public static void main(String[] args){

		Scanner sc= new Scanner(System.in);
		System.out.print("Enter a Number: ");
		int num = sc.nextInt();
		
		int rev = 0;
		int rem=0;
		
		System.out.println("Square of odd digits from a given number: ");

		while(num>0){
			rem = num % 10 ;
			if(rem%2==1){
				System.out.println(rem*rem);
			}
			num= num/10;
		}

	}
}
