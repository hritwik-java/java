import java.util.Scanner;

class Pattern_6{

	public static void main(String[] args){

		Scanner sc =new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			
			int n = row ;
			int ch = 96 + row;

			for(int j=1;j<=i;j++){
				
				if(i%2==0){	
					System.out.print(n-- +" ");
				}else{
					System.out.print((char)ch-- +" ");
				}
			}
			System.out.println();
		}
	}
}
