import java.io.*;

class Pattern_4{
	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader( new InputStreamReader (System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i=1; i<=row; i++){
			int CH = 64+row;
			int ch = 96+row;
			for(int j=1; j<=i; j++){
				if(i%2==0){
					System.out.print((char)CH-- +" ");
				}else{
					System.out.print((char)ch-- +" ");
				}
			}
			System.out.println();
		}
	}
}

