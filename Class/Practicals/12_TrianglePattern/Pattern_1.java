import java.io.*;

class Pattern_1 {
	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader( new InputStreamReader (System.in));

		System.out.print("Enter the number of rows: ");
		int row = Integer.parseInt(br.readLine());
		
		int n = row * row;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){

				System.out.print(n +" ");
			}
			System.out.println();
		}
	}
}

