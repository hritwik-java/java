import java.util.Scanner;

class Program_6 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements of Array: ");

		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();

		}
		
		int prod =1;
		for(int i=0;i<arr.length;i++){

			if(i%2==1){

				prod*=arr[i];
			}
		}

		System.out.println("Product of odd indexed elements is: "+prod);
	}
}


