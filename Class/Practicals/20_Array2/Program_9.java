import java.util.Scanner;

class Program_9 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements of Array: ");

		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();

		}

		int min = arr[0];

		for(int i=0;i<arr.length;i++) {

			if(arr[i]<min){
				min = arr[i];
			}
		}
		System.out.println("Minimum element from array is: "+min);
	}
}

