import java.util.Scanner;

class Program_8 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements of Array: ");

		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();

		}
		System.out.println("Array Elements are: ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]>5 && arr[i]<9){
				System.out.println(arr[i]);
			}
		}
	}
}


