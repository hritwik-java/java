/*4.WAP to search a specific element in an array and return its index. Ask the user to
provide the number to search, also take size and elements input from the user.
*/

import java.util.Scanner;

class Program_4 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements of Array: ");

		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();

		}

		System.out.print("Enter element to search in array: ");
		int search = sc.nextInt();
		int flag = 0;

		for(int i=0;i<arr.length;i++) {

			if(arr[i] == search) {

				System.out.println(search+ " found in array at index: " +i);
				flag=1;
			}
		}
		if(flag==0){
			System.out.println("Element not found in given array.");
		}
	}
}


