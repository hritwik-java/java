import java.util.Scanner;

class Program_7 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements of Array: ");

		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();

		}
		System.out.println("Array Elements: ");

		if(size%2==0) {
			for(int i=0;i<arr.length;i++){
				if(i%2==0){
					System.out.println(arr[i]);
				}
			}
		}else{
			for(int i=0;i<arr.length;i++){
				System.out.println(arr[i]);
			}
		}
	}
}

