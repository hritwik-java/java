/*2.WAP to print the sum of elements divisible by 3 in the array, where you have to take the
size and elements from the user.*/

import java.util.Scanner;

class Program_2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements of Array: ");

		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();

		}

		int sum=0;

		for(int i=0;i<arr.length;i++) {
			if(arr[i]%3==0){
				sum+=arr[i];
			}
		}
		System.out.println("Sum of elements divisible by 3 in the array is: "+sum);
	}
}



