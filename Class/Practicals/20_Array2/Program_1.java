/* 1.WAP to count the even numbers in an array where you have to take the size and
elements from the user. And also print the even numbers too */

import java.util.Scanner;

class Program_1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements of Array: ");

		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();

		}

		int cnt = 0;

		System.out.println("Even elements of Array are: ");

		for(int i=0; i<arr.length; i++) {
			if(arr[i]%2==0){
				System.out.println(arr[i]);
				cnt++;
			}
		}

		System.out.println("Count of Even number in array is: "+cnt);
	}
}
			


