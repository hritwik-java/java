import java.util.Scanner;

class Pattern_6 {
	public static void main(String[] args) {


		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			
			int n=row*row;

			for(int j=1;j<=row;j++){

				if(i%2==1){
					System.out.print(n-- +"\t");
				}else{
					if(j%2==1){
						System.out.print(n +"\t");
						n=n-5;
					}else{
						System.out.print(n +"\t");
					}
				}
			}
			System.out.println();
		}
	}
}


