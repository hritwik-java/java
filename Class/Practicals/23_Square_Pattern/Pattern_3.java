import java.util.Scanner;

class Pattern_3 {
	public static void main(String[] args) {


		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();
		
		int n=row;
		int ch = 96+row;

		for(int i=1;i<=row;i++){

			for(int j=1;j<=row;j++) {

				if(i%2==1){
					if(j%2==1){
						System.out.print((char)ch +"\t");
					}else{
						System.out.print(n +"\t");
					}
				}else{
					if(j%2==1){
						System.out.print(n +"\t");
					}else{
						System.out.print((char)ch +"\t");
					}
				}
				ch++;
				n++;
			}
			System.out.println();
		}
	}
}
