import java.util.Scanner;

class Pattern_1 {
	public static void main(String[] args) {


		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();
		
		int CH = 64+row;
		int ch = 96+row;

		for(int i=1;i<=row;i++){

			for(int j=1;j<=row;j++) {

				if(j==1){
					System.out.print((char)CH +"\t");

				}else{
					System.out.print((char)ch +"\t");

				}
				CH++;
				ch++;
			}
			System.out.println();
		}
	}
}

