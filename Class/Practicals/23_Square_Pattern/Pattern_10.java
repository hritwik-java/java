import java.util.Scanner;

class Pattern_10 {
	public static void main(String[] args) {


		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();
		
		int n=row*row;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){

				if(i==j){
					System.out.print("$\t");
				}else{
					System.out.print(n*j +"\t");
				}
				n--;
			}
			System.out.println();
		}	
	}
}


