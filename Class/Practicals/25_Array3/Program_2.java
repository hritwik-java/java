import java.util.Scanner;

class Program_2 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();

		System.out.print("Enter a number to search in its occurrence in Array: ");
		int search = sc.nextInt();

		for(int i=0;i<arr.length;i++){

			if(arr[i] == search ){

				System.out.println(search+ " found at index "+i );
			}
		}
	}
}


