import java.util.Scanner;

class Program_9 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();

		System.out.println("Composite Numbers in Array Are: ");
		
		for(int i=0;i<arr.length;i++){
			int cnt=0;

			for(int j=1;j<=arr[i];j++){

				if(arr[i]%j==0){

					cnt++;
				}
			}

			if(cnt==2){
				System.out.println(arr[i]);

			}
		}
	}
}
