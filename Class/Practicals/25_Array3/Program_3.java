import java.util.Scanner;

class Program_3 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();

		System.out.print("Enter a Element to search its number of occurrences in Array: ");
		int search = sc.nextInt();
		int cnt=0;
		for(int i=0;i<arr.length;i++){

			if(arr[i] == search ){
			
				cnt++;
			}
		}
			if(cnt>0){
		
				System.out.println(search+ " number occured "+cnt+" times in an Array."  );
			}else{
				System.out.println(search +" not found in Array");
			}

		
	}
}
