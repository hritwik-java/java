import java.util.Scanner;

class Program_7 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();


		for(int i=0;i<arr.length;i++){

			if(arr.length%2==1 && arr.length>=5){

				if(arr[i]%2==1){
					System.out.println(arr[i]);
				}
			}else{
				if(arr[i]%2==0){
					System.out.println(arr[i]);
				}
			}
		}

	}
}
