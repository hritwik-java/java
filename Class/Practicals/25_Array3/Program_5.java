import java.util.Scanner;

class Program_5 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();

		for(int i=0;i<arr.length;i++){

			if(arr[i]<0){

				arr[i] = +arr[i];
				arr[i] = arr[i] *arr[i];
			}
		}
		System.out.print("Array After converting all negative numbers into their squares: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();
				
		
	}
}
