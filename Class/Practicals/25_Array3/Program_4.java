import java.util.Scanner;

class Program_4 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();

		for(int i=0;i<arr.length;i++){

			if(arr[i]%2==0){

				arr[i] = 1;
			}else{
				arr[i] = 0;
			}
		}
		System.out.print("Array After converting odd numbers into 0 and Even numbers into 1: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();
				
		
	}
}
