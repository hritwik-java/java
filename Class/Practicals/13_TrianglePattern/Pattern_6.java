import java.util.*;

class Pattern_6{
	public static void main(String[] args){


		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		int temp=row;

		for (int i=1;i<=row; i++){

			int n=1;
			char ch ='a';

			for (int j=1; j<=temp; j++){

				if( j%2==0){
					System.out.print(ch++ +" ");
				}else{
					System.out.print(n++ +" ");
				}
			}
			System.out.println();
			temp--;
		}
	}
}

