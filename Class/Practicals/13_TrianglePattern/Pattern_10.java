import java.util.*;

class Pattern_10{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		int temp=row;
		for (int i=row;i>=1;i--){

			int CH = 64 + temp;
			int ch = 96 + temp; 

			for(int j=1;j<=i;j++){
				if(i%2==0){
					System.out.print((char)CH +" ");
					CH--;
				}else{
					System.out.print((char)ch +" ");
					ch--;
				}
			}
			System.out.println();
			temp--;
		}
	}
}

