import java.util.*;

class Pattern_8{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		int temp = row;

		for (int i=1;i<=row;i++){
			
			int ch = 64+temp;
			int n=temp;

			for(int j=row;j>=i;j--){
				if(i%2==0){
					System.out.print((char)ch +" ");

				}else{
					System.out.print(n +" ");
				}
				ch--;
				n--;
			}
			System.out.println();
			temp--;
		}
	}
}

