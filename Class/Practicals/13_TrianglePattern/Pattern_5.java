import java.util.*;

class Pattern_5{
	public static void main(String[] args){


		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();


		for (int i=1;i<=row;i++){
			char CH='A';
			char ch='a';
			for(int j=row;j>=i;j--){	
				if(i%2==0){
					System.out.print(ch++ +" ");
				}else{
					System.out.print(CH++ +" ");
				}
			}
			System.out.println();
		}
	}
}

