import java.util.*;

class Pattern_4{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");

		int row = sc.nextInt();

		int n= 64 +(row*(row+1))/2 ;

		for (int i=1;i<=row;i++){
		
			for(int j=row;j>=i;j--){
				System.out.print((char)n +" ");
				n--;
			}
			System.out.println();
		}
	}
}

