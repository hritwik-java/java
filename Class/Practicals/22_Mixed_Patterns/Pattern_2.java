import java.util.Scanner;

class Pattern_2 {
	public static void main(String[] args) {


		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();
		
		int n=1;

		int temp=row;
		for(int i=1;i<=row;i++){

			for(int space=1;space<=i-1;space++){

				System.out.print("\t");
			}
			for(int j=1;j<=temp;j++){

				System.out.print(n++ +"\t");

			}
			n=n-1;
			temp--;
			System.out.println();
		}
	}
}

		
