import java.util.Scanner;

class Pattern_10 {
	public static void main(String[] args) {


		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){

			for(int space=1;space<=row-i;space++) {

				System.out.print("\t");

			}
			int n=i;

			for(int j=1;j<=i*2-1;j++) {

				if(j<i){

					System.out.print(n-- +"\t");

				}else{
					System.out.print(n++ +"\t");

				}
			}

			System.out.println();
		}
	}
}


