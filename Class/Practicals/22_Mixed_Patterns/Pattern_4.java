import java.util.Scanner;

class Pattern_4 {
	public static void main(String[] args) {


		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();
		
		int temp=row;
		
		for(int i=1;i<=row;i++){
			
			char CH = 'A';
			char ch = 'a';

			for(int space=1;space<=i-1;space++){

				System.out.print("\t");
				CH++;
				ch++;


			}
			for(int j=1;j<=temp;j++){

				if(row%2==0) {

					System.out.print(ch++ +"\t");

				}else{
		
					System.out.print(CH++ +"\t");
				
				}

			}
			temp--;

			System.out.println();
		}
	}
}


