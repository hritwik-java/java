import java.util.Scanner;

class Pattern_9 {
	public static void main(String[] args) {


		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();
		
		int n=row;

		for(int i=1;i<=row;i++){

			for(int space=1;space<=row-i;space++){

				System.out.print("\t");

			}

			for(int j=1;j<=i*2-1;j++){

				System.out.print(n-- +"\t");
			}

			n=row;
			n=n+i;

			System.out.println();

		}

	}
}


