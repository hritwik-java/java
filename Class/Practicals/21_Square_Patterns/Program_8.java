import java.util.Scanner;

class Program_8 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);


		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		int n = row;

		for(int i=1;i<=row;i++){
			
			int CH = 64+row;

			for(int j=1;j<=row;j++) {

				if(i%2==1){
					if(j%2==1){
						System.out.print("#\t");
					}else{
						if(j>2){
							System.out.print((char)--CH +"\t");
						}else{
							System.out.print((char)CH +"\t");
						}
					}

				}else{
					if(j%2==1){
						if(j>2){
							System.out.print((char)--CH +"\t");
						}else{
							System.out.print((char)CH +"\t");
						}
					}else{
						System.out.print("#\t");
					}
				}

			}

			System.out.println();
		}
	}
}
