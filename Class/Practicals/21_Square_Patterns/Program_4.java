import java.util.Scanner;

class Program_4 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		int n = row;
		int CH = 64+row;

		for(int i=1;i<=row;i++){

			
			for(int j=1;j<=row;j++){
	
				if(j==1){
				
					System.out.print((char)CH +"\t");
				}else{
					System.out.print(n+"\t");
				}
				n++;
				CH++;
			}
			System.out.println();
		}
	}
}
