import java.util.Scanner;

class Program_9 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);


		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){

			int n=1;

			for(int j=1;j<=row;j++) {

				if(i%2==1){
					if(j%2==1){
						System.out.print(n*2 +"\t");
					}else{
						System.out.print(n*3 +"\t");
					}

				}else{
					if(j%2==1){
					
						System.out.print(n*3 +"\t");
					}else{
						System.out.print(n*2 +"\t");
					}
				}

				n++;
			}

			System.out.println();
		}
	}
}
