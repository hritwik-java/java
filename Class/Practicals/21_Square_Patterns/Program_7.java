import java.util.Scanner;

class Program_7 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);


		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		int n = row;

		for(int i=1;i<=row;i++){
			
			int CH = 64+i;

			for(int j=1;j<=row;j++) {

				if(n%2==0){

					System.out.print(n +"\t");

				}else{
					System.out.print((char)CH +"\t");
				}

				n++;
			}

			System.out.println();
		}
	}
}

