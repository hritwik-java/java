import java.util.Scanner;

class Program_10 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);


		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int n = row;
			int CH = 64+row;

			for(int j=1;j<=row;j++) {

				if(i%2==1){
					if(j%2==1){
						System.out.print(n +"\t");
					}else{
						System.out.print((char)CH +"\t");
					}
				}else{
					System.out.print((char)CH +"\t");
				}
				n--;
				CH--;
			}

			System.out.println();
		}
	}
}
