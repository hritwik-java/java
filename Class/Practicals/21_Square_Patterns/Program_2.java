import java.util.Scanner;

class Program_2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		int n = row;

		for(int i=1;i<=row;i++){

			
			for(int j=1;j<=row;j++){
	
				if(n%3==0){
				
					System.out.print(n*3 +"\t");
				
				}else if(n%5==0){
					
					System.out.print(n*5 +"\t");
				}else{
					System.out.print(n+"\t");
				}
				n++;
			}
			System.out.println();
		}
	}
}
