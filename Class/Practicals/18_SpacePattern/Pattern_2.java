import java.util.Scanner;


class Pattern_2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		for(int i=0;i<=row;i++) {

			for(int space=1;space<=row-i;space++) {

				System.out.print("\t");
			}

			int n = row ;

			for(int j=1;j<=i;j++){

				System.out.print(n-- +"\t");
			}

			System.out.println();
		}
	}
}

