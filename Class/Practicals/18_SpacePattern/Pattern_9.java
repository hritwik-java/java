import java.util.Scanner;

class Pattern_9 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt(); 


		for(int i=1; i<=row; i++) {
			
			for(int space=1; space<=i-1; space++) {

				System.out.print("\t");
			}
			int CH = 64 + row;
			
			for(int j=row; j>=i; j--) {
				
				System.out.print((char)CH-- +"\t");
			}
		System.out.println();
		}	
		
	}
}
