
import java.util.Scanner;

class Pattern_10 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt(); 


		for(int i=1; i<=row; i++) {
			int CH = 65;

			for(int space=1; space<=i-1; space++) {

				System.out.print("\t");
			}
			
			for(int j=row; j>=i; j--) {
				if(j%2==0){

					System.out.print(CH +"\t");
				}else{
					System.out.print((char)CH++ +"\t");
				}
			}
		System.out.println();
		}	
		
	}
}
