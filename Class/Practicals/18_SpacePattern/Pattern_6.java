import java.util.Scanner;

class Pattern_6 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt(); 

		int num = row;

		for(int i=1; i<=row; i++) {
			
			for(int space=1; space<=i-1; space++) {

				System.out.print("\t");

			}
			
			for(int j=row; j>=i; j--) {

				System.out.print(num +"\t");
			}
			System.out.println();
			num--;
		}

	}
}
