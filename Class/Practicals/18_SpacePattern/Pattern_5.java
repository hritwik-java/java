import java.util.Scanner;

class Pattern_5 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt(); 

		for(int i=1;i<=row;i++) {

			for(int space=1;space<=row-i;space++) {

				System.out.print("\t");

			}
			
			for(int j=1;j<=i;j++) {

				System.out.print(i*j +"\t");

			}
			
			System.out.println();
		}

	}
}
