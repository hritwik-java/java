import java.util.Scanner;

class Pattern_3 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of Rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++) {

			for(int space=1;space<=row-i;space++) {

				System.out.print("\t");
			}

			int CH = 65+row-i;

			for(int j=1;j<=i;j++) {

				System.out.print((char)CH +"\t");
				CH++;
			}

		System.out.println();

		}
	}
}

