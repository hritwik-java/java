import java.util.Scanner;

class Pattern_8 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt(); 


		for(int i=1; i<=row; i++) {
			
			for(int space=1; space<=i-1; space++) {

				System.out.print("\t");
			}
			int num=i;
			
			for(int j=row; j>=i; j--) {
				
				System.out.print(num++ +"\t");
			}
		System.out.println();
		}	
		
	}
}
