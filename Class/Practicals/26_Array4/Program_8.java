import java.util.Scanner;

class Program_8 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.next().charAt(0);

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();

		System.out.print("Enter charracter to search its Occurence: ");
		char search = sc.next().charAt(0);
		int cnt=0;

		for(int i=0;i<arr.length;i++){

			if(search==arr[i]){
				cnt++;

			}
		}
		
		System.out.println("Count of "+search+ " is  : "+cnt);
	}
}

