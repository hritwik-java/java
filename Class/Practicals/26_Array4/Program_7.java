import java.util.Scanner;

class Program_7 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.next().charAt(0);

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();

		
		for(int i=0;i<arr.length;i++){

			if(arr[i]>96 && arr[i]<123 ){
				arr[i] =arr[i] + 32;

				System.out.println(arr[i]);
			}else{
				System.out.println(arr[i]);
			}
		}
		
	}
}

