import java.util.Scanner;

class Program_2 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements of array: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = sc.nextInt();

		}

		System.out.print("Array Data: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] +" ");
		}
		System.out.println();
		
		int max = arr[0];
		int min = arr[0];


		for(int i=0;i<arr.length;i++){

			if(min>arr[i]){
				
				min=arr[i];

			}
			if(max<arr[i]){
				max=arr[i];
			}
		}

		int diff = max - min;

		System.out.println("Difference Betwwen Max and Min Element in Arry is: " +diff);

	}
}
