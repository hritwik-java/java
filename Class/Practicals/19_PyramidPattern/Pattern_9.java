import java.util.Scanner;

class Pattern_9 {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt();

		for(int i=1; i<=row; i++) {

			for(int space=1; space<=row-i; space++) {
				System.out.print("\t");
			}

			int CH = 65;
			int ch = 97;

			for(int j=1; j<=i*2-1; j++) {

				if(i%2==1){	
					if(j<i){
						System.out.print((char)CH++ +"\t");
					}else{
						System.out.print((char)CH-- +"\t");
					}
				}else{
					if(j<i){
						System.out.print((char)ch++ +"\t");
					}else{
						System.out.print((char)ch-- +"\t");
					}

				}
			}
			System.out.println();
		}
	}
}

