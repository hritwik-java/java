import java.util.Scanner;

class Pattern_2 {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt();
		
		int n=1;

		for(int i=1; i<=row; i++) {

			for(int space=1; space<=row-i; space++) {

				System.out.print("\t");
			}

			for(int j=1; j<=i*2-1; j++) {
				
				System.out.print(n++ +"\t");
			}
		System.out.println();
		}
	}
}
