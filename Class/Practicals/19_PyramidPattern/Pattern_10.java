
import java.util.Scanner;

class Pattern_10 {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt();

		int CH = 64+row;

		for(int i=1; i<=row; i++) {

			for(int space=1; space<=row-i; space++) {

				System.out.print("\t");
			}

			for(int j=1; j<=i*2-1; j++) {

				if(j<i) {
					System.out.print((char)CH++ +"\t");
				}else{
					System.out.print((char)CH-- +"\t");
				}
			} 
			System.out.println();
		}
	}
}

