import java.util.*;

class Program_4{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a number: ");
		int num = sc.nextInt();

		int fact = 1;

		while(num!=1){
			 fact =fact * num ;

			 num--;
		}

		System.out.println("Factorial of given number is: "+fact);
	}
}
