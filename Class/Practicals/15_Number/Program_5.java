import java.util.*;


class Program_5{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a number to get its Reverse number: ");
		int num = sc.nextInt();

		int temp = num ;
		int rem=0;
		int rev=0;

		while(temp>0){

			rem = temp % 10 ;
			rev = rev *10 + rem;
			temp = temp / 10 ;

		}

		System.out.println("Reverse of "+num+" is : "+rev);
	}
}

