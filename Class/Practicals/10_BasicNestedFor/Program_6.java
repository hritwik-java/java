import java.util.Scanner;

class Program_6{
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt();
			
		int n =1;
		char ch='A';
		for(int i = 1;i<=row; i++){
			n=1;ch='A';
			for(int j=1;j<=row; j++){
				System.out.print(n++ +""+ch++ +" ");
			}
			System.out.println();
		}
	}
}
