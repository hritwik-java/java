import java.util.Scanner;

class Program_4{
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt();

		int ch = 65;

		for(int i = 1;i<=row; i++){
			for(int j=1;j<=row; j++){
				System.out.print((char)ch+" ");
				ch=ch+2;
			}
			System.out.println();
		}
	}
}
