import java.util.Scanner;

class Program_10{
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number of Rows: ");
		int row = sc.nextInt();

		int n=1;
		for(int i = 1;i<=row; i++){
			n=i;
			for(int j=1;j<=row; j++){
				System.out.print(n++ +" ");
			}
			System.out.println();
		}
	}
}
