class LogicalOperator{
	public static void main(String[] args){
		
		int x = 10;
		int y = 12;
	
		System.out.println("Value of x = " +x+ " and y= " +y); 
		System.out.println("Value of ((x > y) && (x < y)) is : " +((x>y)&&(x<y)));
		System.out.println("Value of x = " +x+ " and y= " +y); 

		
		System.out.println("Value of ((++x) >= y) && (x < ++y)) is : " +((++x > y)&&(x<--y)));
		System.out.println("Value of x = " +x+ " and y= " +y); 


		System.out.println("Value of ((x-- <= ++y) && (++x < y--)) is : " +((x-- <= ++y)&&(++x < y--)));
		System.out.println("Value of x = " +x+ " and y= " +y); 


		System.out.println("Value of ((x-- <= ++y) || (++x < y--)) is : " +((x-- <= ++y)||(++x < y--)));
		System.out.println("Value of x = " +x+ " and y= " +y); 
		 
	}
}


