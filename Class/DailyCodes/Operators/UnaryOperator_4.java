
class UnaryOperator{
	public static void main(String[] args){
		int x= 5;
		System.out.println("Value of x = " +x);
		System.out.println("Value of +x = " +(+x));
		System.out.println("Value of -x = " +(-x));
		
		int y = 10;
		System.out.println("Value of y = "+y);
		System.out.println("Value of --y = "+(--y));
		System.out.println("Value of y = "+y);
		
		System.out.println("Value of y-- = "+(y--));
		System.out.println("Value of y = "+y);
		
		System.out.println("Value of ++y = "+(++y));
		System.out.println("Value of y = "+y);
		
		System.out.println("Value of y++ = "+(y++));
		System.out.println("Value of y = "+y);
		
	}
}
