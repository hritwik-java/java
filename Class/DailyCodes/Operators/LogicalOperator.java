class LogicalOperator{
	public static void main(String[] args){

		boolean x = true;
		boolean y = false;
	
		System.out.println("Value of (x) = " +(x));
		System.out.println("Value of (y) = " +(y));
		System.out.println("Value of (x && y) = " +(x&&y));
		System.out.println("Value of (x || y) = " +(x||y));
		System.out.println("Value of (!x) = " +(!x));
	}
}

