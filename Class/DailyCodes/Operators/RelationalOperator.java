class RelationalOperator{
	public static void main(String[] args){
		int x,y;
		x=8;y=8;

		System.out.println("Value of x = "+x);
		System.out.println("Value of y = "+y);

		System.out.println("Value of (x==y) = " + (x==y) );
		System.out.println("Value of (x!=y) = " + (x!=y) );
		System.out.println("Value of (x<y)  = " + (x<y) );
		System.out.println("Value of (x<=y) = " + (x<=y) );
		System.out.println("Value of (x>y)  = " + (x>y) );
		System.out.println("Value of (x>=y) = " + (x>=y) );
		
		int a = 5 ;
		int b =10 ;
		System.out.println("Value of a = " +a);
		System.out.println("Value of b = " +b);

		System.out.println("Value of (a==b) = " + (a==b) );
		System.out.println("Value of (a!=b) = " + (a!=b) );
		System.out.println("Value of (a<b)  = " + (a<b) );
		System.out.println("Value of (a<=b) = " + (a<=b) );
		System.out.println("Value of (a>b)  = " + (a>b) );
		System.out.println("Value of (a>=b) = " + (a>=b) );
	}
}

