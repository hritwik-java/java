import java.io.*;

class Demo{

	public static void main(String[] args) throws IOException {

		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		 System.out.print("Enter size of array: ");
		 int size = Integer.parseInt(br.readLine());

		 int arr[] = new int[size];

		 System.out.println("Enter "+size+" Elements: ");

		 for(int i=0;i<arr.length;i++){

			 arr[i] = Integer.parseInt(br.readLine());

		 }

		 System.out.println("Array Elemnets are: ");

		 for(int i=0;i<arr.length;i++){

			 System.out.println(arr[i]);
		 }
	}
}



