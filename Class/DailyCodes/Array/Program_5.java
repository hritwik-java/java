import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());

		int numArr[] = new int[]{1,2,3,4,5};

		char arr[] = new char[size];

		System.out.println("Enter character elements of array: ");

		for(int i=0;i<arr.length;i++){
	
			arr[i] = br.readLine().charAt(0);
		}

		System.out.println(arr);
		System.out.println(numArr);


	}
}

