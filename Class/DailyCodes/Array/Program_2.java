import java.util.*;

class Program_2{
	public static void main(String[] args){


		Scanner sc = new Scanner(System.in);


		System.out.print("Enter size of Array: ");
		int size =  sc.nextInt();


		int arr[] = new int[size];


		System.out.println("Enter Array Items: ");
		for (int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Array Elements are: ");
		for(int i=0;i<size;i++){
			System.out.println(arr[i]+" ");
		}
		System.out.println("Length of array is : "+arr.length);
	}
}

			
