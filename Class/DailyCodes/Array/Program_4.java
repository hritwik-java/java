import java.io.*;

class Demo {
	
	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());

		float arr[] = new float[size];

		System.out.println("Enter percentage of "+size+" students: ");

		for(int i=0;i<arr.length;i++){

			arr[i] = Float.parseFloat(br.readLine());

		}

		System.out.println("Array DAta: ");

		for(int i=0;i<arr.length;i++){

			System.out.println(arr[i]);
		}
	}
}


