import java.util.Scanner;

class Demo {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int arr[][] = new int[3][];

		arr[0] = new int[3];
		arr[1] = new int[2];
		arr[2] = new int[4];

		System.out.println("Enter Array Elements: ");

		for(int i=0;i<arr.length;i++) {

			System.out.println("Enter elements for row no: "+(i+1));
			for(int j=0;j<arr[i].length;j++) {

				arr[i][j] = sc.nextInt();

			}
		}

		System.out.println();
		System.out.println("Array Elements: ");
		for(int i=0;i<arr.length;i++) {
			for(int j=0;j<arr[i].length;j++) {

				System.out.print(arr[i][j] +"\t");
			}
			System.out.println();
		}
	}
}
