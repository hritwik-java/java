import java.util.Scanner;

class Demo {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number of rows in your Jagged Array: ");
		int row = sc.nextInt();

		int arr[][] = new int[row][];

		for(int i=0;i<arr.length;i++) {

			System.out.println("Enter number of colums in row number: "+ i );

			int col = sc.nextInt();

			arr[i] = new int[col];

		}

		System.out.println();

		for(int i=0;i<arr.length;i++) {
			System.out.println("Enter elements for row number: "+i);

			for(int j=0;j<arr[i].length;j++) {

				arr[i][j] = sc.nextInt();
			}
		}

		System.out.println("Jagged Array : ");

		for(int i=0;i<arr.length;i++) {
			for(int j=0;j<arr[i].length;j++) {

				System.out.print(arr[i][j] +"\t");
			}
			System.out.println();
		}
	}
}



