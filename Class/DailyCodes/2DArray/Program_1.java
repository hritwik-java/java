import java.io.*;

class Demo {
	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter number of rows and Col: ");
		int row = Integer.parseInt(br.readLine());
		int col = Integer.parseInt(br.readLine());

		int arr[][] = new int[row][col];

		System.out.println("Enter Array Data: ");

		for(int i=0;i<arr.length;i++){

			for(int j=0;j<arr[i].length;j++) {

				arr[i][j] = Integer.parseInt(br.readLine());

			}
		}

		System.out.println("Entered array Data: ");

		for(int i=0;i<arr.length;i++) {
			for(int j=0;j<arr[i].length;j++) {

				System.out.print(arr[i][j] +"\t");
			}
			System.out.println();
		}

		System.out.println(arr);
		System.out.println(arr[1]);
		System.out.println(arr[1][1]);

		
	}
}

