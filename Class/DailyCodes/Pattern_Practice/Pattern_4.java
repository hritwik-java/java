class Pattern4{
	public static void main(String[] args){
		
		int n=1;
		for(int i =1;i<=6;i++){
			if(i%2==1){
				System.out.print(n++ + " ");
			}else{
				System.out.print(n*n*n +" ");
				n++;
			}
		}
		System.out.println();
	}
}
