class Program{
	public static void main(String[] args){
		boolean x = true , y =false ;
		System.out.println("First boolean value: "+x);
		System.out.println("Second boolean value: "+y);
		
		System.out.println("Logical AND : "+x+ " && " +y+ " = " +(x && y));
		System.out.println("Logical OR : " +x+ " || " +y+ " = " +(x || y));
		System.out.println("Logical NOT for first value : "+x+ " = " +(!x));
		System.out.println("Logical NOT for second value : " +y+ " = " +(!y));
		
	}
}

