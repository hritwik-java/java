class Program{
	public static void main(String[] args){

		int x =5 ,y=3 ;

		System.out.println("First Integer Number = "+x);
		System.out.println("Second Integer Number = "+y);

		System.out.println("Bitwise AND " +x+ " & " +y+ " = "+(x&y));
		System.out.println("Bitwise OR " +x+ " | " +y+ " = "+(x|y));
		System.out.println("Bitwise XOR " +x+ " ^ " +y+ " = "+(x^y));

		System.out.println("Left shift " +x+ " << 1 = " +(x<<1)); 
		System.out.println("Right shift " +x+ " >> 1 = " +(x>>1));
	}
}	
		

